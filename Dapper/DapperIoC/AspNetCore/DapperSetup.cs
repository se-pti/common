﻿using System;
using Dapper.Contracts;
using Dapper.Main;
using Microsoft.Extensions.DependencyInjection;

namespace DapperIoC.AspNetCore
{
    /// <summary>
    /// Добавить библиотеку для работы с даппером в контейнер зависимостей
    /// </summary>
    public static class DapperSetup
    {
        public static IServiceCollection AddDapper(this IServiceCollection services)
        {
            services.AddSingleton(typeof(IDapperConnectionFactory<>), typeof(AspNetCoreDapperConnectionFactory<>));
            services.AddSingleton(typeof(IDapperTransactionFactory<>), typeof(DapperTransactionFactory<>));
            services.AddScoped(typeof(IDapperContext<>), typeof(DapperContext<>));

            return services;
        }
    }
}
