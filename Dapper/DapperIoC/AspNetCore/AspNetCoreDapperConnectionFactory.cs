﻿using Dapper.Main;
using System;
using System.Collections.Generic;
using System.Text;
using Dapper.Contracts;
using Microsoft.Extensions.Options;

namespace DapperIoC.AspNetCore
{
    /// <summary>
    /// Данный наследник нужен только для того, чтобы можно было использовать механизм внедрения конфигурации в контейнер AspNetCore
    /// </summary>
    public class AspNetCoreDapperConnectionFactory<TConfiguration> : DapperConnectionFactory<TConfiguration> where TConfiguration : class, IDapperConfiguration, new()
    {
        public AspNetCoreDapperConnectionFactory(IOptions<TConfiguration> configuration) : base(configuration.Value)
        {

        }
    }
}
