﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using Dapper.Contracts;
using Dapper.Main;

namespace DapperIoC.Autofac
{
    public static class DapperSetup
    {
        /// <summary>
        /// Добавляет библиотеку для работы с дапперм в контейнер зависимостей
        /// </summary>
        /// <param name="containerBuilder"></param>
        /// <returns></returns>
        public static ContainerBuilder AddDapper(this ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterGeneric(typeof(DapperConnectionFactory<>)).As(typeof(IDapperConnectionFactory<>)).SingleInstance();
            containerBuilder.RegisterGeneric(typeof(DapperTransactionFactory<>)).As(typeof(IDapperTransactionFactory<>)).SingleInstance();
            containerBuilder.RegisterGeneric(typeof(DapperContext<>)).As(typeof(IDapperContext<>)).InstancePerDependency();

            return containerBuilder;
        }
    }
}
