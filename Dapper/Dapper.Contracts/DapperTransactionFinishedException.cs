﻿using System;

namespace Dapper.Contracts
{
    /// <summary>
    /// Данное исключение используется для уведомления пользователей данной библиотеки о том,
    /// что произошла попытка выполнения запроса на транзакции на которой был выполнен Commit или Rollback
    /// </summary>
    public class DapperTransactionFinishedException : Exception
    {
    }
}
