﻿using System;
using System.Data;

namespace Dapper.Contracts
{
    /// <summary>
    /// Данный интерфейс используется для того, чтобы выполнять несколько запросов в базу данных в рамках одной транзакции.
    /// Если явно не был вызван метод Commit транзакция будет откачена.
    /// Транзакции создаются с помощью фабрики <see cref="IDapperTransactionFactory{TConfiguration}"/> и передавать в <see cref="IDapperContext{TConfiguration}"/>
    /// </summary>
    public interface IDapperTransaction : IDisposable
    {
        IDbConnection Connection { get; }
        IDbTransaction DbTransaction { get; }


        void Commit();
        void Rollback();
    }
}