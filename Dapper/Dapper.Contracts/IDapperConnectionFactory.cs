﻿using System.Data;

namespace Dapper.Contracts
{
    /// <summary>
    /// Данный интерфейс позволяет создавать подключения к базе данных на основе модели конфигурации, содержащей строку подключения.
    /// Вместо того чтобы использовать этот класс напрямую лучше обратиться к <see cref="IDapperContext{TConfiguration}"/>
    /// </summary>
    /// <typeparam name="TConfiguration"></typeparam>
    public interface IDapperConnectionFactory<TConfiguration> where TConfiguration : class, IDapperConfiguration, new()
    {
        IDbConnection CreateConnection();
    }
}