﻿namespace Dapper.Contracts
{
    /// <summary>
    /// Можно создавать реализацию данного интерфейса с необходимой строкой подключения и передавать в качестве параметров в <see cref="IDapperContext{TConfiguration}"/>
    /// </summary>
    public interface IDapperConfiguration
    {
        string ConnectionString { get; set; }
    }
}