﻿namespace Dapper.Contracts
{
    /// <summary>
    /// Данный интерфейс позволяет создавать транзакции и подключение к базе данных для нее.
    /// </summary>
    /// <typeparam name="TConfiguration"></typeparam>
    public interface IDapperTransactionFactory<TConfiguration> where TConfiguration : class, IDapperConfiguration, new()
    {
        IDapperTransaction OpenTransaction();
    }
}