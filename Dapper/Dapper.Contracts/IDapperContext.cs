﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Dapper.Contracts
{
    /// <summary>
    /// Данный интерфейс используется для выполнения операций обращения к ьазе данных через даппер.
    /// Он абстрагирует логику выполнения запросов, создания подключения.
    /// </summary>
    /// <typeparam name="TConfiguration"></typeparam>
    public interface IDapperContext<TConfiguration> where TConfiguration : class, IDapperConfiguration, new()
    {
        Task<int> Execute(string sql, object parameters = null, IDapperTransaction transaction = default,
            CancellationToken cancellationToken = default);

        Task<T> ExecuteScalarAsync<T>(string sql, object parameters = null, IDapperTransaction transaction = default,
            CancellationToken cancellationToken = default);

        Task<IEnumerable<T>> QueryAsync<T>(string sql, object parameters = null, IDapperTransaction transaction = default,
            CancellationToken cancellationToken = default);

        Task<T> QueryFirstAsync<T>(string sql, object parameters = null, IDapperTransaction transaction = default,
            CancellationToken cancellationToken = default);

        Task<T> QueryFirstOrDefaultAsync<T>(string sql, object parameters = null, IDapperTransaction transaction = default,
            CancellationToken cancellationToken = default);
    }
}