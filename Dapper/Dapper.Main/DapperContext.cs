﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Dapper.Contracts;
using Dapper;

namespace Dapper.Main
{
    public class DapperContext<TConfiguration> : IDapperContext<TConfiguration> where TConfiguration : class, IDapperConfiguration, new()
    {
        private readonly IDapperConnectionFactory<TConfiguration> _connectionFactory;

        public DapperContext(IDapperConnectionFactory<TConfiguration> connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public Task<int> Execute(string sql, object parameters = null, IDapperTransaction transaction = default,
            CancellationToken cancellationToken = default)
        {
            return Execute((connection, command) => connection.ExecuteAsync(command), sql, parameters,
                transaction, cancellationToken);
        }

        public Task<T> ExecuteScalarAsync<T>(string sql, object parameters = null, IDapperTransaction transaction = default,
            CancellationToken cancellationToken = default)
        {
            return Execute((connection, command) => connection.ExecuteScalarAsync<T>(command), sql, parameters,
                transaction, cancellationToken);
        }

        public Task<IEnumerable<T>> QueryAsync<T>(string sql, object parameters = null, IDapperTransaction transaction = default,
            CancellationToken cancellationToken = default)
        {
            return Execute((connection, command) => connection.QueryAsync<T>(command), sql, parameters,
                transaction, cancellationToken);
        }

        public Task<T> QueryFirstAsync<T>(string sql, object parameters = null, IDapperTransaction transaction = default,
            CancellationToken cancellationToken = default)
        {
            return Execute((connection, command) => connection.QueryFirstAsync<T>(command), sql, parameters,
                transaction, cancellationToken);
        }

        public Task<T> QueryFirstOrDefaultAsync<T>(string sql, object parameters = null, IDapperTransaction transaction = default,
            CancellationToken cancellationToken = default)
        {
            return Execute((connection, command) => connection.QueryFirstOrDefaultAsync<T>(command), sql, parameters,
                transaction, cancellationToken);
        }

        private async Task<TResult> Execute<TResult>(
            Func<IDbConnection, CommandDefinition, Task<TResult>> executor,
            string sql,
            object parameters = null,
            IDapperTransaction transaction = default,
            CancellationToken cancellationToken = default)
        {
            var command = new CommandDefinition(sql, parameters, transaction: transaction?.DbTransaction,
                cancellationToken: cancellationToken);

            TResult result;

            if (transaction == default)
            {
                using var connection = _connectionFactory.CreateConnection();
                result = await executor?.Invoke(connection, command);
            }
            else
            {
                result = await executor?.Invoke(transaction.Connection, command);
            }

            return result;
        }
    }
}
