﻿using System.Data;
using Dapper.Contracts;
using Npgsql;

namespace Dapper.Main
{
    public class DapperConnectionFactory<TConfiguration> : IDapperConnectionFactory<TConfiguration> where TConfiguration : class, IDapperConfiguration, new()
    {
        private readonly TConfiguration _configuration;

        public DapperConnectionFactory(TConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IDbConnection CreateConnection()
        {
            var connection = new NpgsqlConnection(_configuration.ConnectionString);
            connection.Open();
            return connection;
        }
    }
}
