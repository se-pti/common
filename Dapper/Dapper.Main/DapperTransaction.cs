﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Dapper.Contracts;

namespace Dapper.Main
{
    public class DapperTransaction : IDapperTransaction
    {
        private readonly IDbConnection _connection;

        public IDbConnection Connection
        {
            get
            {
                if (IsFinished())
                {
                    throw new DapperTransactionFinishedException();
                }

                return _connection;
            }
        }

        private readonly IDbTransaction _transaction;

        public IDbTransaction DbTransaction
        {
            get
            {
                if (IsFinished())
                {
                    throw new DapperTransactionFinishedException();
                }

                return _transaction;
            }
        }

        private bool _transactionIsCommited = false;
        private bool _transactionIsRollbacked = false;

        public DapperTransaction(IDbConnection connection)
        {
            _connection = connection;
            _transaction = connection.BeginTransaction();
        }

        public void Commit()
        {
            DbTransaction.Commit();
            _transactionIsCommited = true;
        }

        public void Rollback()
        {
            DbTransaction.Rollback();
            _transactionIsRollbacked = true;
        }

        public void Dispose()
        {
            if (!IsFinished())
            {
                _transaction.Rollback();
            }

            _transaction.Dispose();
            _connection.Close();
        }

        private bool IsFinished()
        {
            return _transactionIsCommited || _transactionIsRollbacked;
        }
    }
}
