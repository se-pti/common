﻿using System;
using System.Collections.Generic;
using System.Text;
using Dapper.Contracts;

namespace Dapper.Main
{
    public class DapperTransactionFactory<TConfiguration> : IDapperTransactionFactory<TConfiguration> where TConfiguration : class, IDapperConfiguration, new()
    {
        private readonly IDapperConnectionFactory<TConfiguration> _connectionFactory;

        public DapperTransactionFactory(IDapperConnectionFactory<TConfiguration> connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public IDapperTransaction OpenTransaction()
        {
            var connection = _connectionFactory.CreateConnection();
            return new DapperTransaction(connection);
        }
    }
}
