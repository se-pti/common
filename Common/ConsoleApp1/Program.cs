﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        public static Task<int> GetValue1()
        {
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);//2
            return Task.FromResult(100500);
        }

        public static async Task<int> GetValue2()
        {
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);//3
            return 500100;
        }


        public static Task<int> GetValue3()
        {
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);//4
            return Task.Run(() => 500100);
        }

        static async Task Main(string[] args)
        {
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId); // 1
            var val1 = await GetValue1();
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);//1
            var val2 = await GetValue2();
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);//1
            var val3 = await GetValue3();
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);//1

            Console.ReadLine();
        }
    }
}
