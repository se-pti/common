﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Helpers
{
    public static class XmlSerelizerExtention
    {
        private static readonly ConcurrentDictionary<Type, XmlSerializer> _serializers;

        static XmlSerelizerExtention()
        {
            _serializers = new ConcurrentDictionary<Type, XmlSerializer>();
        }

        public static bool Deserialize<T>(string xml, out T obj, out Exception exception)
        {
            exception = null;
            obj = default(T);
            try
            {
                obj = Deserialize<T>(xml);
                return true;
            }
            catch (Exception e)
            {
                exception = e;
                return false;
            }
        }

        public static bool Deserialize(string xml, out object obj)
        {
            return Deserialize(xml, out obj, out _);
        }

        public static T Deserialize<T>(string xml)
        {
            StringReader stringReader = null;
            try
            {
                stringReader = new StringReader(xml);
                return (T)GetSerializer(typeof(T)).Deserialize(XmlReader.Create(stringReader));
            }
            finally
            {
                stringReader?.Dispose();
            }
        }

        public static string Serialize(this object obj)
        {
            StreamReader streamReader = null;
            MemoryStream memoryStream = null;
            try
            {
                var type = obj.GetType();
                memoryStream = new MemoryStream();
                GetSerializer(type).Serialize(memoryStream, obj);
                memoryStream.Seek(0, SeekOrigin.Begin);
                streamReader = new StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                streamReader?.Dispose();
                memoryStream?.Dispose();
            }
        }

        public static string Serialize(this object obj, Encoding encoding, XmlSerializerNamespaces namespase)
        {
            StreamReader streamReader = null;
            MemoryStream memoryStream = null;
            try
            {
                memoryStream = new MemoryStream();
                GetSerializer(obj.GetType()).Serialize(memoryStream, obj, namespase);
                memoryStream.Seek(0, SeekOrigin.Begin);
                streamReader = new StreamReader(memoryStream, encoding);
                return streamReader.ReadToEnd();
            }
            finally
            {
                streamReader?.Dispose();
                memoryStream?.Dispose();
            }
        }

        private static XmlSerializer GetSerializer(Type type)
        {
            var result = _serializers.GetOrAdd(type, (val) => new XmlSerializer(type));
            return result;
        }
    }
}
