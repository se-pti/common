﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BaseDao.EF
{
    public interface IRepositoryBase<T> where T: class
    {
        void Add(T entity);

        void Delete(T entity);

        void Edit(T entity);

        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);

        IQueryable<T> GetAll();

        IQueryable<T> GetAll(IEnumerable<string> includeObjects);

        void Save();
    }
}