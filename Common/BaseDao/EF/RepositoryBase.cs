﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BaseDao.EF
{
    public abstract class RepositoryBase<TDbContext, TEntity> :
        IDisposable, IRepositoryBase<TEntity>
        where TEntity : class
        where TDbContext : DbContext, new()
    {
        public TDbContext DbContext { get; }

        protected RepositoryBase()
        {
            DbContext = new TDbContext();
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }

        public void Add(TEntity entity)
        {
            DbContext.Set<TEntity>().Add(entity);
        }

        public void Delete(TEntity entity)
        {
            DbContext.Set<TEntity>().Remove(entity);
        }

        public void Edit(TEntity entity)
        {
            DbContext.Entry(entity).State = EntityState.Modified;
        }

        public IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate)
        {
            return DbContext.Set<TEntity>().Where(predicate);
        }

        public IQueryable<TEntity> GetAll()
        {
            return DbContext.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAll(IEnumerable<string> includeObjects)
        {
            var enumerable = includeObjects as IList<string> ?? includeObjects.ToList();
            if (includeObjects == null || !enumerable.Any())
            {
                return GetAll();
            }

            IQueryable<TEntity> result = DbContext.Set<TEntity>();
            return enumerable.Aggregate(result, (current, includeObject) => current.Include(includeObject));
        }

        public void Save()
        {
            DbContext.SaveChanges();
        }
    }
}
