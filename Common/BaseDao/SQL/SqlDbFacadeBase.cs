﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDao.SQL
{
    public abstract class SqlDbFacadeBase
    {
        private readonly string _connectionString;

        protected SqlDbFacadeBase(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<T> ExecuteReader<T>(string query, Func<SqlDataReader, T> func, Tuple<string, object>[] parameters = null)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var command = connection.CreateCommand())
                    {
                        ConfigureCommand(CommandType.Text, query, parameters, command);
                        using (var reader = command.ExecuteReaderAsync())
                        {
                            return func(await reader);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default;
            }
        }

        public async Task<List<T>> ExecuteCollectionReader<T>(string query, Func<SqlDataReader, T> func, Tuple<string, object>[] parameters = null)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var command = connection.CreateCommand())
                    {
                        ConfigureCommand(CommandType.Text, query, parameters, command);
                        using (var reader = command.ExecuteReaderAsync())
                        {
                            var finishedReader = await reader;
                            var result = new List<T>();
                            if (finishedReader?.HasRows == true)
                            {
                                while (finishedReader.Read())
                                {
                                    result.Add(func(finishedReader));
                                }
                            }

                            return result;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default;
            }
        }

        //public async Task<T> ExecuteNonQuery<T>(string query, Func<SqlDataReader, T> func, Tuple<string, object>[] parameters = null)
        //{

        //}

        //public async Task<T> ExecuteScalar<T>(string query, Func<SqlDataReader, T> func, Tuple<string, object>[] parameters = null)
        //{

        //}

        private static void ConfigureCommand(CommandType commandType, string query, Tuple<string, object>[] parameters, SqlCommand command)
        {
            command.CommandText = query;
            command.CommandType = commandType;
            command.CommandTimeout = 1200;
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    command.Parameters.AddWithValue(parameter.Item1, parameter.Item2);
                }
            }
        }
    }
}
