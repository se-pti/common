﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDao.SQL
{
    public static class SqlReaderExcention
    {
        public static long GetLong(this SqlDataReader reader, string fieldName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(fieldName)) ? reader.GetInt64(reader.GetOrdinal(fieldName)) : 0;
        }

        public static long? GetNullableLong(this SqlDataReader reader, string fieldName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(fieldName)) ? reader.GetInt64(reader.GetOrdinal(fieldName)) : (long?)null;
        }

        public static DateTime GetDateTime(this SqlDataReader reader, string fieldName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(fieldName)) ? reader.GetDateTime(reader.GetOrdinal(fieldName)) : DateTime.MinValue;
        }

        public static DateTime? GetNullableDateTime(this SqlDataReader reader, string fieldName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(fieldName)) ? reader.GetDateTime(reader.GetOrdinal(fieldName)) : (DateTime?)null;
        }

        public static int GetInt(this SqlDataReader reader, string fieldName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(fieldName)) ? reader.GetInt32(reader.GetOrdinal(fieldName)) : 0;
        }

        public static int? GetNullableInt(this SqlDataReader reader, string fieldName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(fieldName)) ? reader.GetInt32(reader.GetOrdinal(fieldName)) : (int?)null;
        }

        public static string GetString(this SqlDataReader reader, string fieldName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(fieldName)) ? reader.GetString(reader.GetOrdinal(fieldName)) : String.Empty;
        }

        public static bool GetBool(this SqlDataReader reader, string fieldName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(fieldName)) ? reader.GetBoolean(reader.GetOrdinal(fieldName)) : false;
        }

        public static bool? GetNullableBool(this SqlDataReader reader, string fieldName)
        {
            return !reader.IsDBNull(reader.GetOrdinal(fieldName)) ? reader.GetBoolean(reader.GetOrdinal(fieldName)) : (bool?)null;
        }

    }
}
