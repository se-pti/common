﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Samples;

namespace BaseDao.SQL
{
    public static class SqlReaderHelper
    {
        public static SampleModelUser GetModelUser(SqlDataReader reader)
        {
            var result = new SampleModelUser();
            result.Id = reader.GetLong(nameof(result.Id));
            result.Surname = reader.GetString(nameof(result.Surname));
            result.Name = reader.GetString(nameof(result.Name));
            result.Patronymic = reader.GetString(nameof(result.Patronymic));

            return result;
        }
    }
}
