﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;

namespace DataHelpers
{
    public static class EnumExtensions
    {
        
        public static T GetCustomAttribute<T>(this Enum enumValue)
        {
            var enumType = enumValue.GetType();
            return (T) enumType.GetMember(enumValue.ToString())
                .FirstOrDefault()
                ?.GetCustomAttributes(typeof(T), false)
                .FirstOrDefault();
        }

        public static string GetDescription(this Enum value)
        {
            return value.GetCustomAttribute<DescriptionAttribute>()
        }
    }
}
